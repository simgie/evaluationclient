﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace EvaluationClient
{

    /*
     *  TODO:
     * 
     */

    /*
     * NOTES:
     *      Meiste Zeit für Asyncrone Tasks aufgewandt, da ohne diese das Fenster einfrieren würde
     *          => Deshalb eigene Async Methoden, weswegen ein Log auch über "Dispatcher.BeginInvoke()" ausgeführt werden muss.
     * 
     */


    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Console console = new Console();
        private List<string> _log = new List<string>();
        private ConnDialog conn_win;
        private Connection connection;
        private int rb = 0, qid = 1; //rb = Radio-Box Checked, qid = Frage-ID

        public MainWindow()
        {
            InitializeComponent();
            console.tb.Document.Blocks.Clear();
            log("Starte Anwendung...");
            bt_trennen.IsEnabled = false;
            connection = new Connection(this);
        }

        
        /// <summary>
        /// Komplettes Programm beenden, wenn das Hauptfenster geschlossen wird
        /// </summary>
        private void Window_Closed(object sender, EventArgs e)
        {
            Application.Current.Shutdown();
        }


        /// <summary>
        /// Zeige Connection-Dialog-Window nachdem das Objekt konstruiert wurde (Event)
        /// </summary>
        private void showConn(object sender, RoutedEventArgs e)
        {
            conn_win = new ConnDialog(this);
            conn_win.ShowInTaskbar = false;
            conn_win.Show();
            conn_win.Owner = this;
        }

        /// <summary>
        /// TitleBar_MouseDown - Drag it.
        /// </summary>
        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && !conn_win.IsVisible)
                this.DragMove();
        }
        private void TitleBarText_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left && !conn_win.IsVisible)
                this.DragMove();
        }

        /// <summary>
        /// CloseButton_Clicked
        /// </summary>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        /// <summary>
        /// SettingsButton_Clicked
        /// </summary>
        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            openLog();
            if (debug.Opacity != 0) debug.Opacity = 0;
            else debug.Opacity = 1;
        }

        
        /// <summary>
        /// Verbinden Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            string tb = tb_ip.Text;
            int port;
            try
            {
                port = Convert.ToInt32(tb.Substring(tb.LastIndexOf(':') + 1));
            }
            catch (System.FormatException)
            {
                log("Bitte einen Port mit \":\" angeben.");
                return;   
            }
            var ip = tb.Remove(tb.IndexOf(port.ToString()) - 1, port.ToString().Length + 1);
            new Task(() => connection.LoopConnect(ip, port)).Start();
        }

        /// <summary>
        /// Trennen Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            log("Verbindung wurde getrennt.");
            connection.ResetConnection();
        }


        /// <summary>
        /// Do-Action Button
        /// </summary>
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            connection.doTask(tb_action.Text);
        }


        private void openLog()
        {
            if (!console.IsLoaded)
            {
                console = new Console();
                console.Show();
                _log.ForEach(delegate(string name)
                {
                    log(name, true);
                });
            }
            else
                console.Focus();
        }

        /// <summary>
        /// Text zum Logfile hinzufügen
        /// </summary>
        /// <param name="t">Text</param>
        /// <param name="_each">Hinzufügen von Text zum Array _log verhindern. (z.B. in einem ForEach-Loop)</param>
        /// </summary>
        public void log(string t, bool _each = false)
        {
            string msg = "["+DateTime.Now.ToLongTimeString()+"]   " + t;
            if (!_each) _log.Add(msg);
            if (_each) msg = t;
            console.tb.AppendText(msg);
            console.tb.AppendText("\n");
            console.tb.ScrollToEnd();
        }

        /// <summary>
        /// ASYNCRON: Text zum Logfile hinzufügen
        /// </summary>
        /// <param name="t">Text</param>
        /// <param name="_each">Hinzufügen von Text zum Array _log verhindern. (z.B. in einem ForEach-Loop)</param>
        /// </summary>
        public void asyncLog(string t, bool _each = false)
        {
            Dispatcher.BeginInvoke(new Action(() =>
                {
                    log(t, _each);
                })
            );
        }


        /// <summary>
        /// Verbindungsaufbau im Loop via Connection-Klasse
        /// </summary>
        /// <param name="ip">IP-Adresse des Hosts</param>
        /// <param name="port">Port des Servers</param>
        public void connect(string ip, int port)
        {
            connection.LoopConnect(ip, port);
        }

        private void displayQuestion(int q)
        {

            tb_question.Text = connection.getQuestion(q);
        }

        /// <summary>
        /// Verändert den Status-Text am unteren Bildschirmrand und bei CONNECTED wird erste Frage Angefordert
        /// </summary>
        /// <param name="status">Status-Parameter</param>
        public void changeStatus(Status status)
        {
            if(status == Status.CONNECTED)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    lb_status.Content = "verbunden";
                    lb_status.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#FFA5D6A7"));
                    bt_trennen.IsEnabled = true;
                    displayQuestion(qid);
                }));
            }
            else if(status == Status.DISCONNECTED)
            {
                Dispatcher.BeginInvoke(new Action(() =>
                {
                    lb_status.Content = "nicht verbunden";
                    lb_status.Foreground = (SolidColorBrush)(new BrushConverter().ConvertFrom("#F44336"));
                    bt_trennen.IsEnabled = false;
                }));
            }
        }

        /// <summary>
        /// Wenn Weiter Button gelickt wird Ergebnisse Senden + neue Frage Anfordern
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bt_weiter_clicked(object sender, RoutedEventArgs e)
        {
            if(rb == 0)
            {
                MessageBox.Show("Bitte eine Möglichkeit auswählen.", "Fehler");
                return;
            }
            connection.SendAnswer(qid, rb);
            qid++;
            rb = 0;
            rb01.IsChecked = false;
            rb02.IsChecked = false;
            rb03.IsChecked = false;
            rb04.IsChecked = false;
            displayQuestion(qid);
        }


        /// <summary>
        /// Wenn RadioButton ausgewählt wird, wird dessen ID in einer Variablen gespeichert
        /// </summary>
        private void rb01_Checked(object sender, RoutedEventArgs e) { rb = 1; }

        private void rb02_Checked(object sender, RoutedEventArgs e) { rb = 2; }

        private void rb03_Checked(object sender, RoutedEventArgs e) { rb = 3; }

        private void rb04_Checked(object sender, RoutedEventArgs e) { rb = 4; }
    }


    // Enum für Verbindungs-Status
    public enum Status
    {
        CONNECTED,
        DISCONNECTED
    }
}
