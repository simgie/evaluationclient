﻿using System.Windows;
using System.Windows.Input;

namespace EvaluationClient
{
    /// <summary>
    /// Interaction logic for Console.xaml
    /// Test
    /// </summary>
    public partial class Console : Window
    {
        public Console()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TitleBar_MouseDown - Drag it.
        /// </summary>
        private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
        private void TitleBarText_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }

        /// <summary>
        /// CloseButton_Clicked
        /// </summary>
        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
