﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EvaluationClient
{
    class Connection
    {
        private bool connected = false;
        private Task send_worker;
        private Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private MainWindow gui;
        private int id;

        // Get Time Button
        private bool cancel_SendLoop = false;

        public Connection(MainWindow gui)
        {
            this.gui = gui;
        }


        

        /// <summary>
        /// Führe eine Aktion aus
        /// </summary>
        /// <param name="task">Aktions-Typ</param>
        public void doTask(string task)
        {

            if (!connected)
            {
                gui.log("Client nicht verbunden!");
                return;
            }
            else
            {
                if (task == "get time")
                {
                    if (send_worker != null)
                    {
                        cancel_SendLoop = true;
                        send_worker = null;
                        gui.log("Stoppe Aktion \"get time\"");
                        return;
                    }
                    else
                    {
                        gui.log("Führe Aktion \"get time\" aus");
                        cancel_SendLoop = false;
                        send_worker = new Task(SendLoop);
                        send_worker.Start();
                    }
                }
                else
                {
                    gui.log("Führe Aktion \"" + task + "\" aus");
                    Send(task);
                }
            }
        }

        public string getQuestion(int id)
        {
            string ret = Send("q" + id);
            if (ret == "e;outofq;")
                return "Die Evaluation wurde beendet. Vielen Dank."; // TODO: BEENDEN
            if (ret != null)
                return ret;
            else return "Fehler (Frage wurde auf dem Server nicht gefunden)";
        }

        /*private async void asyncConnect()
        {
            Task<bool> returnedResult = LoopConnect();
            bool connected = await returnedResult;
            if (connected)
                new Task(SendLoop).Start();
        }*/

        /// <summary>
        /// Verbindung zum Server rücksetzen
        /// </summary>
        public void ResetConnection()
        {
            _clientSocket.Close();
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            send_worker = null;
            connected = false;
            gui.changeStatus(Status.DISCONNECTED);
        }

        /// <summary>
        /// Dem bereits Verbundenen Server eine Nachricht senden
        /// </summary>
        /// <param name="req">Nachricht</param>
        private string Send(string req)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(req);
            try
            {
                _clientSocket.Send(buffer);

                byte[] recievedBuf = new byte[1024];
                int rec = _clientSocket.Receive(recievedBuf);
                byte[] data = new byte[rec];
                Array.Copy(recievedBuf, data, rec);
                string recived = Encoding.ASCII.GetString(data);
                if (recived.Contains("auml;")) recived = recived.Replace("auml;", "ä");
                gui.asyncLog("Recived: " + recived);
                return recived;
            }
            catch (SocketException)
            {
                gui.asyncLog("Verbindung zum Server verloren...");
                ResetConnection();
                return null;
            }


        }


        /// <summary>
        /// Dauerhaftes senden zum Server (bis cancel_SendLopp = false)
        /// </summary>
        private void SendLoop()
        {
            while (true)
            {
                Thread.Sleep(500);

                if (cancel_SendLoop)
                    break;
                string req = "get time";
                byte[] buffer = Encoding.ASCII.GetBytes(req);
                try
                {
                    _clientSocket.Send(buffer);

                    byte[] recievedBuf = new byte[1024];
                    int rec = _clientSocket.Receive(recievedBuf);
                    byte[] data = new byte[rec];
                    Array.Copy(recievedBuf, data, rec);
                    gui.asyncLog("Recived: " + Encoding.ASCII.GetString(data));
                }
                catch (SocketException)
                {
                    gui.asyncLog("Verbindung zum Server verloren...");
                    ResetConnection();
                    return;
                }

            }
        }

        /// <summary>
        /// Antwort an Server senden
        /// </summary>
        /// <param name="qid">Frage-ID</param>
        /// <param name="rb">Antwort-ID</param>
        public void SendAnswer(int qid, int rb)
        {
            doTask(id + "-a:" + qid + ":" + rb);
        }

        /// <summary>
        /// Verbindungsaufbau im Loop
        /// </summary>
        /// <param name="ip">IP-Adresse des Hosts</param>
        /// <param name="port">Port des Servers</param>
        public void LoopConnect(string ip, int port)
        {
            if (connected)
            {
                gui.asyncLog("Es ist bereits eine Verbindung aktiv!");
                return;
            }
            int attempts = 0, maxattempts = 50;
            IPAddress host;
            try
            {
                host = Dns.GetHostAddresses(ip)[0];
            }
            catch (SocketException e)
            {
                gui.asyncLog("Fehler beim Auflösen des Hosts: \"" + e.Message + "\"");
                return;
            }
            gui.asyncLog("Versuche Verbindung mit \"" + ip + ":" + port + "\" aufzubauen, mit maximal " + maxattempts + " Versuchen.");
            while (!_clientSocket.Connected)
            {
                try
                {
                    if (attempts >= maxattempts)
                    {
                        gui.asyncLog("Verbindung nach " + attempts + " Versuchen nicht erfolgreich. Beende...");
                        return;
                    }
                    attempts++;
                    _clientSocket.Connect(host, port);
                }
                catch (SocketException)
                {
                    gui.asyncLog("Verbindungsversuch: " + attempts);
                }
            }
            connected = true;
            id = Convert.ToInt32(Send("handshake"));
            gui.changeStatus(Status.CONNECTED);
            gui.asyncLog("Verbunden");
        }

    }
}
