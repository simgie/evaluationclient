﻿using System;
using System.Threading.Tasks;
using System.Windows;

namespace EvaluationClient
{
    /// <summary>
    /// Interaktionslogik für Connection.xaml
    /// </summary>
    public partial class ConnDialog : Window
    {
        private MainWindow mw;

        public ConnDialog(MainWindow mw)
        {
            InitializeComponent();
            this.mw = mw;
        }

        private void bt_conn_Click(object sender, RoutedEventArgs e)
        {
            string tb = tb_ip.Text;
            int port;
            try
            {
                port = Convert.ToInt32(tb.Substring(tb.LastIndexOf(':') + 1));
            }
            catch (System.FormatException)
            {
                mw.log("Bitte einen Port mit \":\" angeben.");
                return;
            }
            var ip = tb.Remove(tb.IndexOf(port.ToString()) - 1, port.ToString().Length + 1);
            new Task(() =>  mw.connect(ip, port)).Start();
            this.Hide();
        }
    }
}
